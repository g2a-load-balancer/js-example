import { EmptyListOfHostsError } from "../errors";
import type { Host } from "../Host";

export class SequentialStrategy {
  private lastLength = 0;
  private pointer = 0;

  selectHost(hosts: Host[]): Host {
    if (!hosts || hosts.length === 0) {
      throw new EmptyListOfHostsError();
    }
    if (this.lastLength !== hosts.length) {
      this.lastLength = hosts.length;
      this.pointer = 0;
    }
    if (this.pointer >= hosts.length) {
      this.pointer = 0;
    }

    const host = hosts[this.pointer];
    this.pointer++;

    return host;
  }
}
