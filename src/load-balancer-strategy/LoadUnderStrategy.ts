import { EmptyListOfHostsError } from "../errors";
import type { Host } from "../Host";

export class LoadUnderStrategy {
  constructor(private readonly maximumLoad: number) {}

  selectHost(hosts: Host[]): Host {
    if (!hosts || hosts.length === 0) {
      throw new EmptyListOfHostsError();
    }

    return (
      hosts.find((h) => h.getLoad() <= this.maximumLoad) ||
      this.findLowest(hosts)
    );
  }

  private findLowest(hosts: Host[]): Host {
    return hosts.reduce(
      (prev, curr) => (prev.getLoad() > curr.getLoad() ? curr : prev),
      hosts[0]
    );
  }
}
