import type { Host, Request } from "./Host";

export interface LoadBalancerStrategy {
  selectHost(hosts: Host[]): Host;
}

export class LoadBalancer implements Host {
  constructor(
    private readonly hosts: Host[],
    private readonly strategy: LoadBalancerStrategy
  ) {}

  getLoad(): number {
    return Math.random();
  }

  async handleRequest(request: Request): Promise<void> {
    return this.strategy.selectHost(this.hosts).handleRequest(request);
  }
}
