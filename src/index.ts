import type { Host, Request } from "./Host";
import {
  LoadUnderStrategy,
  SequentialStrategy,
} from "./load-balancer-strategy";
import { LoadBalancer } from "./LoadBalancer";

export const SequentialLoadBalancer = (hosts: Host[]) =>
  new LoadBalancer(hosts, new SequentialStrategy());
export const LoadUnderLoadBalancer = (hosts: Host[], load = 0.75) =>
  new LoadBalancer(hosts, new LoadUnderStrategy(load));

export type { Host, Request };
export { LoadUnderStrategy, SequentialStrategy, LoadBalancer };
export * from "./errors";
