export class EmptyListOfHostsError extends Error {
  constructor() {
    super("Cannot process further with an empty list of hosts");
  }
}
