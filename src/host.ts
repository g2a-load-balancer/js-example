export type Request = unknown;

export type Host = {
  getLoad(): number;
  handleRequest(request: Request): Promise<void>;
};
