import { EmptyListOfHostsError, LoadBalancer } from "../src";
import { applyHostMocks } from "./load-balancer-strategy/applyHostMocks";

describe("Load balancer", () => {
  const [hosts, clearHosts] = applyHostMocks(4);
  const strategy = { selectHost: jest.fn() };
  const loadBalancer = new LoadBalancer(hosts, strategy);

  beforeEach(() => {
    clearHosts();
    strategy.selectHost.mockReset();
  });

  it("should return select host using strategy", async () => {
    const handleRequest = jest.fn();
    strategy.selectHost.mockReturnValue({ handleRequest });

    await loadBalancer.handleRequest({});
    expect(strategy.selectHost).toHaveBeenCalled();
    expect(handleRequest).toHaveBeenCalled();
  });

  it("should pass throwed error further", async () => {
    const handleRequest = jest
      .fn()
      .mockRejectedValue(new EmptyListOfHostsError());
    strategy.selectHost.mockReturnValue({ handleRequest });

    try {
      await loadBalancer.handleRequest({});
      throw new Error("should throw an error");
    } catch (e) {
      expect(strategy.selectHost).toHaveBeenCalled();
      expect(handleRequest).toHaveBeenCalled();
      expect(e).toBeInstanceOf(EmptyListOfHostsError);
    }
  });
});
