import { EmptyListOfHostsError, LoadUnderStrategy } from "../../src";
import { applyHostMocks } from "./applyHostMocks";

describe("LoadBalancer under load strategy", () => {
  const [hosts, hostsReset, hostsApplyLoad] = applyHostMocks(4);
  const strategy = new LoadUnderStrategy(0.75);

  beforeEach(() => {
    hostsReset();
  });

  it("should select the first host with load under 0.75", () => {
    hostsApplyLoad(0.8, 0.8, 0.7, 0.6);
    const host = strategy.selectHost(hosts);
    expect(host).toBe(hosts[2]);
  });

  it("should select the lowest load when all hosts have the load above 0.75", () => {
    hostsApplyLoad(0.85, 0.9, 0.9, 0.76);
    const host = strategy.selectHost(hosts);
    expect(host).toBe(hosts[3]);
  });

  it("should throw an error when list of hosts is empty", () => {
    try {
      strategy.selectHost([]);
      throw new Error("should throw an error");
    } catch (e) {
      expect(e).toBeInstanceOf(EmptyListOfHostsError);
    }
  });
});
