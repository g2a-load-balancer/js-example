type hostMock = {
  index: number;
  getLoad: jest.Mock<number>;
  handleRequest: jest.Mock<Promise<void>>;
};
type clearMockFn = () => void;
type applyLoadFn = (...loads: number[]) => void;

export function applyHostMocks(
  amountOfHosts: number
): [hostMock[], clearMockFn, applyLoadFn] {
  const hosts: hostMock[] = [];
  for (let i = 0; i < amountOfHosts; i++) {
    hosts[i] = {
      index: i,
      getLoad: jest.fn(),
      handleRequest: jest.fn(),
    };
  }

  const clearMocks = () => {
    for (const host of hosts) {
      host.getLoad.mockReset();
      host.handleRequest.mockReset();
    }
  };

  const applyLoad = (...loadPerHost: number[]) => {
    hosts.forEach((host, index) => {
      host.getLoad.mockReturnValue(loadPerHost[index] || 1);
    });
  };

  return [hosts, clearMocks, applyLoad];
}
