import { EmptyListOfHostsError, SequentialStrategy } from "../../src";
import { applyHostMocks } from "./applyHostMocks";

describe("LoadBalancer sequential strategy", () => {
  const [hosts, hostsReset] = applyHostMocks(4);
  const strategy = new SequentialStrategy();

  beforeEach(() => {
    hostsReset();
  });

  describe("should select each host sequentially", () => {
    for (let i = 0; i < hosts.length; i++) {
      it(`should select host no ${i}`, () => {
        const host = strategy.selectHost(hosts);
        expect(host).toBe(hosts[i]);
      });
    }

    it("should select host no 0 when hit the last host", () => {
      const host = strategy.selectHost(hosts);
      expect(host).toBe(hosts[0]);
    });
  });

  it("should throw an error when list of hosts is empty", () => {
    try {
      strategy.selectHost([]);
      throw new Error("should throw an error");
    } catch (e) {
      expect(e).toBeInstanceOf(EmptyListOfHostsError);
    }
  });
});
